import HomeScreen from "../screens/ListScreen";
import { createStackNavigator, createAppContainer } from 'react-navigation';
import InfoScreen from "../screens/InfoScreen"; // Version can be specified in package.json

const RootStack = createStackNavigator(
    {
        List: HomeScreen,
        Details: InfoScreen,
    },
    {
        initialRouteName: 'List',
    }
);
export default createAppContainer(RootStack);